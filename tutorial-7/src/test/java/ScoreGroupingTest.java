import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


public class ScoreGroupingTest {
    private Map<Integer, List<String>> group;

    @Before
    public void setUp() {
        Map<String, Integer> scores = new HashMap<>();

        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);

        group = ScoreGrouping.groupByScores(scores);
    }

    @Test
    public void testGroupKeyByCommonValue() {
        assertTrue(group.get(12).contains("Alice"));
        assertTrue(group.get(15).contains("Bob"));
        assertTrue(group.get(11).contains("Charlie"));
        assertTrue(group.get(15).contains("Delta"));
        assertTrue(group.get(15).contains("Emi"));
        assertTrue(group.get(11).contains("Foxtrot"));
    }
}
