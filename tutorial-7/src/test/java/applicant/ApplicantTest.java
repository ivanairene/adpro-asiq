package applicant;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class ApplicantTest {
    private Applicant applicant;

    @Before
    public void setUp() {
        applicant = new Applicant();
    }

    @Test
    public void testCheckCredit() {
        assertTrue(CreditEvaluator.getPredicate().test(applicant));
    }

    @Test
    public void testCheckCriminalRecord() {
        assertFalse(CriminalRecordsEvaluator.getPredicate().test(applicant));
    }

    @Test
    public void testCheckEmployment() {
        assertTrue(EmploymentEvaluator.getPredicate().test(applicant));
    }

    @Test
    public void testCheckQualified() {
        assertTrue(QualifiedEvaluator.getPredicate().test(applicant));
    }
}
