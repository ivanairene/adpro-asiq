/**
 * 1st exercise.
 */
import java.util.stream.IntStream;

public class PrimeChecker {
    public static boolean isPrime(int number) {
        return number > 1
                && IntStream.range(2, number)
                .noneMatch(i -> number % i == 0);
    }
}
