import static java.util.stream.Collectors.groupingBy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 3rd exercise.
 */
public class ScoreGrouping {

    public static Map<Integer, List<String>> groupByScores(
            Map<String, Integer> scores) {
        return scores.keySet().stream().collect(groupingBy(scores::get));

    }
}
