package applicant;

import java.util.function.Predicate;

public class CreditEvaluator extends EvaluatorChain {

    public CreditEvaluator(Evaluator next) {
        super(next);
    }

    public boolean evaluate(Applicant applicant) {
        if (applicant.getCreditScore() > 600) {
            return super.evaluate(applicant);
        }

        return false;
    }

    public static Predicate<Applicant> getPredicate() {
        return thisApplicant -> thisApplicant.getCreditScore() > 600;
    }
}
