package applicant;

import java.util.function.Predicate;

public class EmploymentEvaluator extends EvaluatorChain {

    public EmploymentEvaluator(Evaluator next) {
        super(next);
    }

    public boolean evaluate(Applicant applicant) {
        if (applicant.getEmploymentYears() > 0) {
            return super.evaluate(applicant);
        }

        return false;
    }

    public static Predicate<Applicant> getPredicate() {
        return theApplicant -> theApplicant.getEmploymentYears() > 0;
    }
}
