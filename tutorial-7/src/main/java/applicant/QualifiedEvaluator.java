package applicant;

import java.util.function.Predicate;

public class QualifiedEvaluator implements Evaluator {

    public boolean evaluate(Applicant applicant) {
        return applicant.isCredible();
    }

    public static Predicate<Applicant> getPredicate() {
        return Applicant::isCredible;
    }
}
