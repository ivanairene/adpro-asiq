package applicant;

import java.util.function.Predicate;

/**
 * 4th exercise.
 */
public class Applicant {

    public boolean isCredible() {
        return true;
    }

    public int getCreditScore() {
        return 700;
    }

    public int getEmploymentYears() {
        return 10;
    }

    public boolean hasCriminalRecord() {
        return true;
    }

    public static boolean evaluate(Applicant applicant, Predicate<Applicant> evaluator) {
        return evaluator.test(applicant);
    }

    private static void printEvaluation(boolean result) {
        String msg = "Result of evaluating applicant: %s";
        msg = result ? String.format(msg, "accepted") : String.format(msg, "rejected");

        System.out.println(msg);
    }


    public static void main(String[] args) {
        Applicant applicant = new Applicant();

        Predicate<Applicant> qualifiedCheck = QualifiedEvaluator.getPredicate();
        Predicate<Applicant> creditCheck = CreditEvaluator.getPredicate();
        Predicate<Applicant> employmentCheck = EmploymentEvaluator.getPredicate();
        Predicate<Applicant> crimeCheck = CriminalRecordsEvaluator.getPredicate();

        printEvaluation(qualifiedCheck.and(creditCheck).test(applicant));
        printEvaluation(qualifiedCheck.and(employmentCheck).and(creditCheck).test(applicant));
        printEvaluation(qualifiedCheck.and(employmentCheck).and(crimeCheck).test(applicant));
        printEvaluation(
                qualifiedCheck
                        .and(employmentCheck)
                        .and(creditCheck)
                        .and(crimeCheck)
                        .test(applicant)
        );
    }
}
