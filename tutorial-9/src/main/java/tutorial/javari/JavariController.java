package tutorial.javari;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;
import tutorial.javari.animal.Message;
import tutorial.javari.animal.Parser;

@RestController
public class JavariController {
    private static Parser pr;

    // Get all animals
    @RequestMapping(value = "/javari", method = RequestMethod.GET)
    public ResponseEntity<?> animals() throws IOException {
        if (pr == null) {
            pr = new Parser();
        }
        if (pr.getAllAnimals().isEmpty()) {
            String message = String.format("No animal available");
            return new ResponseEntity<Message>(new Message(message), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<Animal>>(pr.getAllAnimals(), HttpStatus.OK);
    }

    // Get animal by id
    @RequestMapping(value = "/javari/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> animal(@PathVariable("id") int id) throws IOException {
        if (pr == null) {
            pr = new Parser();
        }
        if (pr.getAnimalById(id) == null) {
            String message = String.format("No animal by this id found!");
            return new ResponseEntity<Message>(new Message(message), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Animal>(pr.getAnimalById(id), HttpStatus.OK);
    }

    // Create new animal
    @RequestMapping(value = "/javari", method = RequestMethod.POST)
    public ResponseEntity<Animal> createAnimal(
            @RequestBody Map<String, String> animalAttr) throws IOException {
        if (pr == null) {
            pr = new Parser();
        }
        int id = Integer.parseInt(animalAttr.get("id"));
        String type = animalAttr.get("type");
        String name = animalAttr.get("name");
        Gender gender = Gender.parseGender(animalAttr.get("gender"));
        double length = Double.parseDouble(animalAttr.get("length"));
        double weight = Double.parseDouble(animalAttr.get("weight"));
        Condition condition = Condition.parseCondition(animalAttr.get("condition"));

        Animal newAnimal = new Animal(id, type, name, gender, length, weight, condition);
        pr.createAnimal(newAnimal);

        return new ResponseEntity<Animal>(newAnimal, HttpStatus.OK);
    }

    // Delete animal by id
    @RequestMapping(value = "/javari/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteAnimal(@PathVariable("id") int id) throws IOException {
        if (pr == null) {
            pr = new Parser();
        }
        if (pr.getAnimalById(id) == null) {
            String message = String.format("No animal by this id found!");
            return new ResponseEntity<Message>(new Message(message), HttpStatus.NOT_FOUND);
        }
        pr.deleteAnimal(id);
        return new ResponseEntity<Animal>(HttpStatus.NO_CONTENT);
    }
}
