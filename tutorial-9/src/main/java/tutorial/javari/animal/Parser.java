package tutorial.javari.animal;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.core.io.ClassPathResource;

public class Parser {
    private static List<Animal> animals;
    private static String fileLocation = "data/mockanimal.csv";
    private static ClassPathResource classPathResource;

    public Parser() throws IOException {
        animals = new ArrayList<>();
        classPathResource = new ClassPathResource(fileLocation);
        CSVReader reader = new CSVReader(new InputStreamReader(classPathResource.getInputStream()));
        reader.readNext();

        String[] currentLine = reader.readNext();

        while (currentLine != null) {
            System.out.println(Arrays.toString(currentLine));
            int id = Integer.parseInt(currentLine[0]);
            String type = currentLine[1];
            String name = currentLine[2];
            Gender gender = Gender.parseGender(currentLine[3]);
            double length = Double.parseDouble(currentLine[4]);
            double weight = Double.parseDouble(currentLine[5]);
            Condition condition = Condition.parseCondition(currentLine[6]);
            animals.add(new Animal(id, type, name, gender, length, weight, condition));
            currentLine = reader.readNext();
        }
        reader.close();
    }

    // Get all animals
    public List<Animal> getAllAnimals() throws IOException {
        return animals;
    }

    // Get information about an animal by its id
    public Animal getAnimalById(int id) {
        for (Animal animal : animals) {
            if (animal.getId() == id) {
                return animal;
            }
        }
        return null;
    }

    // Create new animal
    public void createAnimal(Animal animal) throws IOException {
        animals.add(animal);
        CSVWriter writer = new CSVWriter(
                new FileWriter(
                        classPathResource.getFile().getPath(), true));
        String newAnimal =
                animal.getId() + "," + animal.getType() + ","
                        + animal.getName() + "," + animal.getGender() + ","
                        + animal.getLength() + "," + animal.getWeight() + ","
                        + animal.getCondition();
        String[] propList = newAnimal.split(",");
        writer.writeNext(propList, false);
        writer.flush();
        writer.close();
    }

    // Delete animal
    public void deleteAnimal(int id) throws IOException {
        Animal toDelete = getAnimalById(id);
        animals.remove(toDelete);
        String content = "id,type,name,gender,length,weight,condition\n";
        for (Animal animal : animals) {
            String condition = animal.getCondition().equals("HEALTHY") ? "healthy" : "not healthy";
            content += animal.getId() + "," + animal.getType() + ","
                    + animal.getName() + "," + animal.getGender() + ","
                    + animal.getLength() + "," + animal.getWeight() + ","
                    + condition + "\n";
        }
        System.out.println(content);
        FileWriter fw = new FileWriter(classPathResource.getFile());
        PrintWriter fout = new PrintWriter(fw, true);
        fout.print(content);
        fout.close();
    }

}
