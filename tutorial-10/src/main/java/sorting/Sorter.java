package sorting;


public class Sorter {

    /**
     * Some sorting algorithm that possibly the slowest algorithm.
     *
     * @param inputArr array of integer that need to be sorted.
     * @return a sorted array of integer.
     */

    public static int[] slowSort(int[] inputArr) {
        int temp;
        for (int i = 1; i < inputArr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (inputArr[j] < inputArr[j - 1]) {
                    temp = inputArr[j];
                    inputArr[j] = inputArr[j - 1];
                    inputArr[j - 1] = temp;
                }
            }
        }
        return inputArr;
    }

    public static int[] quickSort(int[] numbers, int low, int high) {
        int i = low;
        int j = high;
        int pivot = numbers[(low + high) / 2];

        // Divide into 2 parts (left & right pivot)
        while (i <= j) {

            // If now value of i is less than pivot, then i
            // become next from the left part
            while (numbers[i] < pivot) {
                i++;
            }

            // If now value of i is more than pivot, then i
            // become next from the right part
            while (numbers[j] > pivot) {
                j--;
            }

            // If we have found value from the left that
            // is smaller than pivot and value from the right
            // that is more that the pivot, then we switch that value
            // Then increase i and decrease j.
            if (i <= j) {
                int temp = numbers[i];
                numbers[i] = numbers[j];
                numbers[j] = temp;
                i++;
                j--;
            }
        }
        // Recursive
        if (low < j) {
            quickSort(numbers, low, j);
        }
        if (i < high) {
            quickSort(numbers, i, high);
        }
        return numbers;
    }

}
