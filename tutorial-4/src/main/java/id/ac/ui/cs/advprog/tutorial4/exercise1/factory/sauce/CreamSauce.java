package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class CreamSauce implements Sauce {
    public String toString() {
        return "Cream Sauce";
    }
}
