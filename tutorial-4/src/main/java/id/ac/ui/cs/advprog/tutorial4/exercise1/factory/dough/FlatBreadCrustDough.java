package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class FlatBreadCrustDough implements Dough {
    public String toString() {
        return "Flat Bread style flat bread crust dough";
    }
}
