package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class RedCheese implements Cheese {

    public String toString() {
        return "Red Cheese";
    }
}
